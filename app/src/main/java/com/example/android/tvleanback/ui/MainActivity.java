/*
 * Copyright (c) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.tvleanback.ui;

import android.app.UiModeManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.android.tvleanback.R;
import com.example.android.tvleanback.model.Video;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/*
 * MainActivity class that loads MainFragment.
 */
public class MainActivity extends LeanbackActivity {

    private UiModeManager uiModeManager;
    public static boolean isTV = false;
    public static Integer tvID = 0;
    public static DatabaseReference mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Comprobamos tipo de dispositivo
        uiModeManager = (UiModeManager) this.getSystemService(UI_MODE_SERVICE);
        isTV = uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION;

        // This is the first time running the app, let's go to onboarding
        startActivity(new Intent(this, OnboardingActivity.class));

        if (isTV && sharedPreferences.getBoolean(OnboardingFragment.COMPLETED_ONBOARDING, false)) {
            channelChanger(mDatabase);
        } else {
            System.out.println("Is on mobile");
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent mainActivity = new Intent(this, MainActivity.class);
    }

    private void channelChanger(DatabaseReference mDatabase) {
        Intent intent = new Intent(this, PlaybackActivity.class);

        mDatabase.child("video").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Video video = snapshot.getValue(Video.class);

                if (video != null){
                    intent.putExtra(VideoDetailsActivity.VIDEO, video);
                    //mDatabase.child("video").getRef().removeValue();
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println(error.getMessage());
            }
        });
    }
}
