package com.example.android.tvleanback.ui;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.example.android.tvleanback.R;

public class EmailPasswordActivity extends FragmentActivity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_emailpassword);
    }
}
