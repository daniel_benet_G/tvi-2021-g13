/*
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.tvleanback.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.leanback.app.GuidedStepFragment;
import androidx.leanback.widget.GuidanceStylist;
import androidx.leanback.widget.GuidedAction;
import android.text.InputType;
import android.widget.Toast;

import com.example.android.tvleanback.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class AuthenticationActivity extends Activity {
    private static final int CONTINUE = 2;
    private static final int REGISTER = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (null == savedInstanceState) {
            GuidedStepFragment.addAsRoot(this, new FirstStepFragment(), android.R.id.content);
        }
    }

    public static class FirstStepFragment extends GuidedStepFragment {
        private FirebaseAuth mAuth;

        @Override
        public int onProvideTheme() {
            return R.style.Theme_Example_Leanback_GuidedStep_First;
        }

        @Override
        @NonNull
        public GuidanceStylist.Guidance onCreateGuidance(@NonNull Bundle savedInstanceState) {

            // Initialize Firebase Auth
            mAuth = FirebaseAuth.getInstance();

            // Check if user is logged in
            FirebaseUser currentUser = mAuth.getCurrentUser();
            System.out.println("Hi ha un user logged? " +  currentUser);

            String title = getString(R.string.pref_title_screen_signin);
            String description = getString(R.string.pref_title_login_description);
            Drawable icon = getActivity().getDrawable(R.drawable.ic_main_icon);
            return new GuidanceStylist.Guidance(title, description, "", icon);
        }

        @Override
        public void onCreateActions(@NonNull List<GuidedAction> actions, Bundle savedInstanceState) {
            GuidedAction enterEmail = new GuidedAction.Builder()
                    .title(getString(R.string.pref_title_username))
                    .descriptionEditable(true)
                    .build();
            GuidedAction enterPassword = new GuidedAction.Builder()
                    .title(getString(R.string.pref_title_password))
                    .descriptionEditable(true)
                    .descriptionInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT)
                    .build();
            GuidedAction login = new GuidedAction.Builder()
                    .id(CONTINUE)
                    .title("LOGIN")
                    .build();
            GuidedAction register = new GuidedAction.Builder()
                    .id(REGISTER)
                    .title("REGISTER")
                    .build();
            actions.add(enterEmail);
            actions.add(enterPassword);
            actions.add(login);
            actions.add(register);
        }

        @Override
        public void onGuidedActionClicked(GuidedAction action) {

            if (action.getId() == CONTINUE) {

                // Extract information from fields
                List<GuidedAction> actions = getActions();
                String email = actions.get(0).getDescription().toString();
                String password = actions.get(1).getDescription().toString();

                // Proceed to sign In
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    FirebaseUser user = mAuth.getCurrentUser();

                                    // Assume the user was logged in
                                    Toast.makeText(getActivity(), "Welcome!", Toast.LENGTH_SHORT).show();
                                    getActivity().finishAfterTransition();
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(getActivity(), "This user doesn't exist!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


            }

            if (action.getId() == REGISTER) {

                // Extract information from fields
                List<GuidedAction> actions = getActions();
                String email = actions.get(0).getDescription().toString();
                String password = actions.get(1).getDescription().toString();

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    FirebaseUser user = mAuth.getCurrentUser();

                                    // Assume the user was logged in
                                    Toast.makeText(getActivity(), "Welcome!", Toast.LENGTH_SHORT).show();
                                    getActivity().finishAfterTransition();
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(getActivity(), "Authentication failed!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        }
    }
}
