from Video import Video

""" 
nueva estructura a formar de la clase TV_Links:
    -category: nombre categoria
    -videos: (x videos de la categoria)
        -description: XXXXXX
        -sources: link_source mp4
        -card: imagen jpg
        -background: imagen jpg
        -title: XXXX
        -studio: XXXX
"""


class TvLinks:
    videos = []
    category = None

    def __init__(self, json):
        self.category = json.get('name')
        self.videos = []
        channels = json.get('channels')

        # description, sources, card, background, title, studio
        for channel in channels:
            if len(channel.get('options')) > 0:
                descripcion = channel.get('web')
                sources = [((channel.get('options'))[0]).get('url')]
                card = channel.get('logo')
                background = channel.get('logo')
                title = channel.get('name')
                studio = channel.get('epg_id')

                video = Video(descripcion, sources, card, background, title, studio)
                self.videos.append(video)
