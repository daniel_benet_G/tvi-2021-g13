from urllib.request import urlopen
import json
from TvLinks import TvLinks
import ssl

ssl._create_default_https_context = ssl._create_unverified_context


class finalJson:
    def __init__(self, links):
        self.links = links

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


def create_new_json(links):
    path = './'# path donde guardar el nuevo JSON a crear
    json_final = finalJson(links)
    jsonstr = json_final.toJSON()

    with open(path + r"\new_json_final.txt", "w", encoding='utf-8') as new_json:
        new_json.write(jsonstr)
    new_json.close()


if __name__ == '__main__':
    tv_links = []
    url = "https://www.tdtchannels.com/lists/tv.json"

    # store the response of URL
    response = urlopen(url)

    # storing the JSON response from url in data
    data_json = json.loads(response.read())

    categories = (data_json.get('countries'))[0]
    for category in categories.get('ambits'):
        links = None
        links = TvLinks(category)
        tv_links.append(links)

    categories = (data_json.get('countries'))[1]
    for category in categories.get('ambits'):
        links = None
        links = TvLinks(category)
        tv_links.append(links)

    create_new_json(tv_links)
