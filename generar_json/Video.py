class Video:
    description = None
    sources = None
    card = None
    background = None
    title = None
    studio = None

    def __init__(self, description, sources, card, background, title, studio):
        self.description = description
        self.sources = sources
        self.card = card
        self.background = background
        self.title = title
        self.studio = studio
